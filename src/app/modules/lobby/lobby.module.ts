import { LoadUsersHttpService } from './services/load-users-http.service';
import {NgModule} from '@angular/core';
import {LobbyMainComponent} from './components/lobby-main/lobby-main.component';
import {LobbyRoutingModule} from './routing/lobby-routing.module';
import {SharedModule} from '../shared/shared.module';
import {LobbyUsersComponent} from './components/lobby-users/lobby-users.component';
import {LobbyNavComponent} from './components/lobby-nav/lobby-nav.component';
import {LobbyUserItemComponent} from './components/lobby-user-item/lobby-user-item.component';
import {LobbyResourcesComponent} from './components/lobby-resources/lobby-resources.component';
import { LobbyUsersModel } from './models/lobby-users.model';

@NgModule({
  declarations: [LobbyMainComponent, LobbyUsersComponent, LobbyNavComponent, LobbyUserItemComponent, LobbyResourcesComponent],
  imports: [
    SharedModule,
    LobbyRoutingModule
  ],
  providers: [
    /*Http Services*/
    LoadUsersHttpService,
    /*Models*/
    LobbyUsersModel
  ]
})
export class LobbyModule {
}
