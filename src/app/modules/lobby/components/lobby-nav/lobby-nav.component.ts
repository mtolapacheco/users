import { AuthenticationService } from './../../../shared/services/authentication.service';
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-lobby-nav',
  templateUrl: './lobby-nav.component.html',
  styleUrls: ['./lobby-nav.component.scss']
})
export class LobbyNavComponent implements OnInit {

  constructor(private authService: AuthenticationService, private router: Router) {
  }

  ngOnInit() {
  }

  public logout(): void {
    this.authService.removeToken();
    this.router.navigate(['']);
  }

}
