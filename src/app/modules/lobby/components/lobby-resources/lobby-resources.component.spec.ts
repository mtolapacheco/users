import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyResourcesComponent } from './lobby-resources.component';

describe('LobbyResourcesComponent', () => {
  let component: LobbyResourcesComponent;
  let fixture: ComponentFixture<LobbyResourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LobbyResourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyResourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
