import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LobbyUsersComponent} from '../lobby-users/lobby-users.component';

describe('LobbyUsersComponent', () => {
  let component: LobbyUsersComponent;
  let fixture: ComponentFixture<LobbyUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LobbyUsersComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
