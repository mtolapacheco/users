import { AuthenticationService } from './../../../shared/services/authentication.service';
import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FADE_IN_OUT } from '../../../shared/animations/animations';
import { LoginHttpService } from './../../services/login-http.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { emailValidator } from './validators/email.validator';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-secure-login',
  templateUrl: './secure-login.component.html',
  styleUrls: ['./secure-login.component.scss'],
  animations: [FADE_IN_OUT]
})
export class SecureLoginComponent implements OnDestroy {

  // public email: FormControl;
  // public password: FormControl;

  public loginForm: FormGroup;
  public alertMessage: string;

  public thereAreErrors: boolean;

  public readonly validDomains = ['gmail', 'hotmail', 'reqres'];
  public readonly passMinLength = 6;

  private timeout;
  private loginSubscription: Subscription;

  private readonly HARCODED_USER = {
    email: 'eve.holt@reqres.in',
    password: 'cityslicka'
  };

  constructor(private router: Router, private formBuilder: FormBuilder,
              private authService: AuthenticationService,
              private loginHttpService: LoginHttpService) {
    this.thereAreErrors = false;
    this.timeout = null;
    this.loginForm = this.formBuilder.group({
      email: [
        this.HARCODED_USER.email,
        [
          Validators.required,
          emailValidator(this.validDomains)
        ]
      ],
      password: [
        this.HARCODED_USER.password,
        [
          Validators.required,
          Validators.minLength(this.passMinLength)
        ]
      ]
    });
  }

  ngOnDestroy(): void {
    this.clearTimeout();
    this._unsubscribe(this.loginSubscription);
  }

  public onSubmit(): void {
    if (this.loginForm.valid) {
      this.loginSubscription =
        this.loginHttpService.doPost(this.loginForm.value).subscribe(
          (response: { token: string }) => {
            // console.log(response.token);
            this.authService.setToken(response.token);
            this.router.navigate(['lobby']);
          },
          () => {
            this.showAlert('User or password is not valid.');
          }
        );
    } else {
      this.showAlert('You must fix the errors to proceed.');
    }
  }
  private showAlert(content: string): void {
    this.alertMessage = content;
    this.clearTimeout();
    this.thereAreErrors = true;

    this.timeout = setTimeout(() => this.thereAreErrors = false, 2000);
  }

  private clearTimeout(): void {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
  }
  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }
}
