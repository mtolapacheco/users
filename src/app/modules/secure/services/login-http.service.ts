import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/bootstrap/http.service';

import { LoginRequest } from './body-request/login.request';

@Injectable()
export class LoginHttpService extends HttpService {

  constructor(private _injector: Injector) {
    super();
  }

  public path(): string {
    return '/api/login';
  }

  public doPost(request: LoginRequest): Observable<object> {
    return this.httpClient().post(this.getUrl(), request);
  }

  protected injector(): Injector {
    return this._injector;
  }
}
