import {Routes} from '@angular/router';
import {SecureMainComponent} from '../components/secure-main/secure-main.component';
import {SecureLoginComponent} from '../components/secure-login/secure-login.component';

export const SECURE_ROUTES: Routes = [
  {
    path: '',
    component: SecureMainComponent,
    children: [
      {
        path: 'login',
        component: SecureLoginComponent
      },
      {
        path: 'sign-up',
        component: SecureLoginComponent
      },
      {
        path: '',
        redirectTo: '/secure/login',
        pathMatch: 'full'
      }
    ]
  }
];
