import {animate, AnimationTriggerMetadata, style, transition, trigger} from '@angular/animations';

export const FADE_IN_OUT: AnimationTriggerMetadata = trigger('chatFadeInOut', [
    transition(':enter', [
      style({opacity: 0}),
      animate('400ms', style({opacity: 1}))
    ]),
    transition(':leave', [
      style({opacity: 1}),
      animate('400ms', style({opacity: 0}))
    ])
  ]
);
