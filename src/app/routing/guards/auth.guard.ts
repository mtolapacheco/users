import { AuthenticationService } from './../../modules/shared/services/authentication.service';
import {Injectable} from '@angular/core';
import {CanLoad, Route, UrlSegment, Router} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  constructor(private authService: AuthenticationService, private router: Router) {

  }

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    const token = this.authService.getToken();

    if (token) {
      return true;
    }
    this.router.navigate(['secure', 'login']);
    return false;
  }
}
